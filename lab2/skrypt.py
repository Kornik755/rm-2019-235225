#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import matplotlib.patches as patches
import matplotlib.animation as animation
from matplotlib import rc
from matplotlib.path import Path
from scipy.spatial import distance
from enum import Enum


# In[2]:


def add_obstacle(x, y, width, height):
    for i in np.arange(_scene.shape[0]):
        for j in np.arange(_scene.shape[1]):
            if j >= x and j <= x + width and i >= y and i <= y + height:
                _scene[j][i] = -1


def add_position(x, y):
    if x < 0 or x >= x_size or y < 0 or y >= y_size or scene[x][y] == -1:
        raise Exception("Wjechales w przeszkode: (" + str(x) + "," + str(y) + ")")
    global x_pos
    global y_pos
    positions[0].append(x)
    positions[1].append(y)
    scene[x][y] = 1
    x_pos = x
    y_pos = y


def move_x(val):
    move(val, 1, 0)


def move_y(val):
    move(val, 0, 1)


def move(val, x, y):
    for i in np.arange(0, abs(val)):
        if val > 0:
            add_position(x_pos + x, y_pos + y)
        else:
            add_position(x_pos - x, y_pos - y)


def update(i):
    line.set_data(
        [x + 0.5 for x in positions[0][0:i]], [y + 0.5 for y in positions[1][0:i]]
    )
    return (line,)


def coverage():
    full_size = len(scene.flatten())
    obst_size = np.count_nonzero(scene == -1)
    cleaned_size = np.count_nonzero(scene == 1)
    dirty_size = np.count_nonzero(scene == 0)
    coveraged_size = cleaned_size / (full_size - obst_size) * 100

    print("Koncowe polozenie: (" + str(x_pos) + ", " + str(y_pos) + ")")
    print("Pelna scena: " + str(full_size))
    print("Przeszkody: " + str(obst_size))
    print("Posprzatane pole: " + str(cleaned_size))
    print("Zasmiecone pole: " + str(dirty_size))
    print("Pokrycie: " + str(round(coveraged_size, 2)) + "%")


# In[3]:


delta = 1
x_size = 40
y_size = 40
x_pos = 0
y_pos = 25
positions = [[], []]
x = y = np.arange(0, 40.0, delta)
X, Y = np.meshgrid(x, y)
_scene = X * 0
_scene[0][25] = 1  # punkt poczatkowy
add_obstacle(0, 0, 4, 16)
add_obstacle(25, 32, 15, 8)
add_obstacle(25, 12, 4, 12)
scene = _scene.copy()
add_position(x_pos, y_pos)


# In[4]:


# Uzupelnic kodem
animacja = True
#######################


# In[5]:


class Robot:
    directions = [(1, 0), (0, 1), (-1, 0), (0, -1)]
    cur_dir = 0

    def turn_r(self):
        if self.cur_dir == 3:
            self.cur_dir = 0
        else:
            self.cur_dir += 1

    def turn_l(self):
        if self.cur_dir == 0:
            self.cur_dir = 3
        else:
            self.cur_dir -= 1

    def turn_back(self):
        if self.cur_dir == 2:
            self.cur_dir = 0
        elif self.cur_dir == 3:
            self.cur_dir = 1
        else:
            self.cur_dir += 2

    def move_straight(self):
        move_x(self.directions[self.cur_dir][1])
        move_y(self.directions[self.cur_dir][0])

    def move_back(self):
        move_x(-self.directions[self.cur_dir][1])
        move_y(-self.directions[self.cur_dir][0])


def s_alg():
    bot = Robot()
    count = 1
    global x_pos
    global y_pos
    while True:
        try:
            bot.move_straight()
        except Exception:

            if count % 2 == 1:
                bot.turn_r()
                count += 1
                try:
                    bot.move_straight()
                    bot.turn_r()
                except Exception:
                    bot.turn_l()
                    bot.turn_back()
                    continue

            else:
                count += 1
                bot.turn_l()
                try:
                    bot.move_straight()
                    bot.turn_l()
                except Exception:
                    bot.turn_r()
                    bot.turn_back()
                    continue

            continue
        if x_pos == 40 and y_pos == 0:
            break
        elif count > 500:
            break


# In[6]:


s_alg()


# In[7]:


_scene[x_pos][y_pos] = 1
_scene = np.where(scene == 0, -0.5, _scene)

plt.show()
fig = plt.figure()
ax = fig.add_subplot(111)
line, = ax.plot([], [], linewidth=3)
plt.imshow(
    _scene.transpose(), cmap=cm.RdYlGn, origin="lower", extent=[0, x_size, 0, y_size]
)
ax.set_title("‘S’ shape pattern algorithm")
plt.grid(True)
rc("animation", html="html5")
if animacja:
    ani = animation.FuncAnimation(fig, update, interval=1, save_count=1)
else:
    plt.plot(positions[0], positions[1], linewidth=3)

coverage()

plt.show()
